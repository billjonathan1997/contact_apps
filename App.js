import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import ContactList from './src/views/ContactList'
import ContactInput from './src/views/ContactInput'
import ContactDetail from './src/views/ContactDetail'

const Stack = createNativeStackNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerStyle:{backgroundColor:'skyblue'}}}>
        <Stack.Screen 
          name="ContactList" 
          component={ContactList} 
          options={{title:'Contact'}}/>
        <Stack.Screen 
          name="ContactInput" 
          component={ContactInput}
          options={({ route })=>({title:route.params.isAddNew?"New Contact":"Edit"})} />
        <Stack.Screen 
          name="ContactDetail" 
          component={ContactDetail} 
          options={{title:"Detail"}}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App
