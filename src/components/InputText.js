import React from 'react'
import { View, Text, TextInput, StyleSheet } from 'react-native'

const InputText = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{props.title} {props.required && <Text style={{color:'red'}}>*</Text>}</Text>
            <TextInput {...props} style={styles.textInput} ></TextInput>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        padding:5
    },
    textInput:{
        width:'100%',
        backgroundColor:'white',
        borderRadius:10,
        marginTop:5,
        paddingHorizontal:10,
        color:'black'
    },
    title:{
        fontWeight:'bold',
        fontSize:18
    }
})

export default InputText
