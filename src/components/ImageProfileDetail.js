import React, {useState, useEffect} from 'react'
import { View, Image, StyleSheet } from 'react-native'

const ImageProfileDetail = (props) => {
    const {image} = props

    const [Error, setError] = useState(false)

    useEffect(() => {
        setError(false)
    }, [image])

    return (
        <View style={styles.imageContainer}>
            <Image 
                style={styles.imageProfile}
                source={{uri:Error||!image?'https://freepikpsd.com/media/2019/10/logo-contact-person-png-Transparent-Images.png':image}} 
                onError={()=>setError(true)}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    imageContainer:{
        width:'100%',
        height:undefined,
        aspectRatio:2,
        elevation:2,
        backgroundColor:'white',
        zIndex:0
    },
    imageProfile:{
        width:'100%',
        height: '100%',
        resizeMode:'cover'
    }
})


export default ImageProfileDetail
