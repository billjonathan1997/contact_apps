import React from 'react'
import { View, Text, Pressable } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native';

const ButtonBack = () => {
    const navigation = useNavigation()
    return (
        <Pressable onPress={()=>navigation.goBack()}style={{margin:10}}>
            <Icon 
                name="arrow-circle-left" 
                size={30} 
                color="black"
                ></Icon>
        </Pressable>
    )
}

export default ButtonBack
