import React, {useState, useEffect} from 'react'
import { View, Text, Image, StyleSheet, Pressable } from 'react-native'
import { useNavigation } from '@react-navigation/native';

const ContactItem = (props) => {
    const { data } = props
    const navigation = useNavigation();

    const [image, setImage] = useState(data.photo)
    
    useEffect(()=>{
        setImage(data.photo)
    },[data.photo])
    
    return (
        <Pressable 
            style={styles.container} 
            onPress={()=>navigation.navigate('ContactDetail',{id:data.id})}
        >
            <View style={styles.viewContact}>
                <Image 
                    style={styles.profilePhoto} 
                    source={{uri:image}} 
                    onError={()=>setImage('https://freepikpsd.com/media/2019/10/logo-contact-person-png-Transparent-Images.png')}
                />

                <Text style={styles.text}> {data.firstName} {data.lastName}</Text>
            </View>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    container:{
        padding:5,
        width:'100%',
        borderBottomWidth:1
    },
    viewContact:{
        padding:10,
        borderRadius:20,
        backgroundColor:'#F2F2F2',
        flexDirection:'row',
        alignItems:'center'
    },
    profilePhoto:{
        height:60,
        width:60,
        borderRadius:30,
        marginRight:20,
        borderWidth:5
    },
    text:{
        fontSize:20,
        fontWeight:'bold'
    }
})
export default ContactItem
