import Axios from 'axios'
import { Alert } from 'react-native'

const instance = Axios.create({
    baseURL: 'https://simple-contact-crud.herokuapp.com/',
    timeout: 6000,
    headers:{'Content-Type': 'application/json'}
});

instance.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    let message = error.response.data.message
    console.log(message)
    if(error.response.status === 400){
      let NewMessage = message.substring(
        message.lastIndexOf("[") + 1, 
        message.lastIndexOf("]")
      )
      message = NewMessage?NewMessage:message
    }
    Alert.alert("Error Message", message?message:'something wrong')
    return Promise.reject(error);
  });

export default instance