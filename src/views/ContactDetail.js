import React, {useEffect,useState} from 'react'
import { View, Text, StyleSheet, Pressable, ActivityIndicator } from 'react-native'
import { useRoute, useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';

import ImageProfileDetail from '../components/ImageProfileDetail'
import ButtonBack from '../components/ButtonBack'
import Http from '../http/Instance'

const ContactDetail = (props) => {
    const route = useRoute()
    const navigation = useNavigation();
    const {id} = route.params

    const [loading, setLoading] = useState(true)
    const [data, setData] = useState({
        firstName:"",
        lastName:"",
        age:0,
        photo:""
    })

    useEffect(()=>{
        getContactByID()
    },[])

    const getContactByID = () => {
        Http.get('contact/'+id)
        .then((response)=>{
            setData(response.data.data)
        })
        .catch((err)=>{
            navigation.goBack()
        })
        .finally(()=>{
            setLoading(false)
        })
    }

    const deleteContact = () => {
        console.log(id)
        Http.delete('contact/'+id)
        .then((response)=>{
            navigation.goBack()
        }).catch((err)=>{

        })
    }
    return (
        <View style={{flex:1}}>
            <ImageProfileDetail image={data.photo}>
                <ButtonBack></ButtonBack>
            </ImageProfileDetail>
            {
                loading?
                <ActivityIndicator size="small"/>
                :
                <View style={{flex:1, justifyContent:'space-between'}}>
                    <View style={styles.containerDetail}>
                        <Text style={styles.nameText}>{data.firstName} {data.lastName}</Text>
                        <Text style={styles.ageText}>{data.age}</Text>
                    </View>

                    <View style={styles.containerButton}>
                        <Pressable style={styles.button} onPress={()=>navigation.navigate('ContactInput',{data:data, isAddNew:false})}>
                            <Icon name="edit" size={28}></Icon>
                            <Text style={styles.ageText}>Edit</Text>
                        </Pressable>

                        <Pressable style={styles.button} onPress={deleteContact}>
                            <Icon name="trash" size={28}></Icon>
                            <Text style={styles.ageText}>Delete</Text>
                        </Pressable>
                    </View>
                </View>
            }
            
            
            
        </View>
    )
}

const styles = StyleSheet.create({
    containerDetail:{
        padding:20
    },
    containerButton:{
        flexDirection:'row', 
        justifyContent:'center'
    },
    nameText:{
        fontSize: 22,
        fontWeight: 'bold'
    },
    ageText:{
        fontSize: 18,
    },
    button:{
        margin:20,
        alignItems:'center'
    }
})

export default ContactDetail
