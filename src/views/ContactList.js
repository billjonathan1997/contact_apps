import React, {useEffect, useState} from 'react'
import { View, Text, Alert, FlatList, Pressable } from 'react-native'
import { useNavigation, useFocusEffect } from '@react-navigation/native';

import Http from '../http/Instance'
import ContactItem from '../components/ContactItem'
import Icon from 'react-native-vector-icons/FontAwesome';

const data = {
    firstName:"",
    lastName:"",
    age:"",
    photo:""
}

const ContactList = () => {
    const [contactList, setContactList] = useState([])
    const [loading, setLoading] = useState(true)
    const navigation = useNavigation();

    useFocusEffect(
        React.useCallback(() => {
            sendGetRequest()
        }, [])
    )

    const sendGetRequest = () => {
        setLoading(true)
        Http.get('contact')
        .then((response)=>{
            setContactList(response.data.data)
        })
        .catch((err)=>{
            Alert.alert(err)
        })
        .finally(()=>setLoading(false))
    };

    const renderItem = ({item}) =>{
        return <ContactItem data={item}/>
    }

    return (
        <View style={{flex:1}}>
            <FlatList
                refreshing={loading}
                onRefresh={sendGetRequest}
                data={contactList}
                renderItem={renderItem}
                keyExtractor={(item)=>item.id}
            />
            <Pressable 
                onPress={()=>navigation.navigate('ContactInput', {data, isAddNew:true})}
                style={{position:'absolute', right:20, bottom:20}}>
                <Icon 
                    name="plus-circle" 
                    size={60}
                    color="skyblue"/>
            </Pressable>
        </View>
    )
}

export default ContactList
