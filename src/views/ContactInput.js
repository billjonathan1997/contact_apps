import React , {useState} from 'react'
import { View, Text, Pressable, Image, StyleSheet, ScrollView, Alert } from 'react-native'
import { useNavigation, useRoute } from '@react-navigation/native';

import ImageProfileDetail from '../components/ImageProfileDetail'
import InputText from '../components/InputText'
import Http from '../http/Instance'

const ContactInput = (props) => {
    const route = useRoute()
    const navigation = useNavigation();
    const { data, isAddNew } = route.params

    const [firstName, setFirstName] = useState(data.firstName)
    const [lastName, setLastName] = useState(data.lastName)
    const [age, setAge] = useState(data.age+"")
    const [image, setImage] = useState(data.photo)
    

    const onSubmit = () => {
        if(!firstName || !lastName || !age || !image ){
            Alert.alert("Validation","Fill out all Fields")
        }else{
            const send = {
                firstName:firstName,
                lastName:lastName,
                age:parseInt(age),
                photo:image
            }
            if(isAddNew){
                sendNewContact(send)
            }else{
                sendEditContact(send)
            }
        }
    }

    const sendNewContact = (items) => {
        Http.post('contact', items)
        .then((response)=>{
            console.log(response);
            navigation.goBack();
        })
        .catch((err)=>{
            console.log(err.response)
        })
    };

    const sendEditContact = (items) => {
        Http.put('contact/'+data.id, items)
        .then((response)=>{
            navigation.pop(2);
        })
        .catch((err)=>{
            console.log(err.response)
        })
    };
    

    return (
        <ScrollView>
            <View style={{flex:1}}>
                <ImageProfileDetail image={image}></ImageProfileDetail>
                <View style={{padding:10}}>
                    <InputText 
                        required
                        title="First Name"
                        value={firstName} 
                        onChangeText={(text)=>setFirstName(text)}
                    />

                    <InputText 
                        required
                        title="Last Name"
                        value={lastName} 
                        onChangeText={(text)=>setLastName(text)}
                    />

                    <InputText 
                        required
                        title="Age"
                        value={age} 
                        keyboardType="numeric" 
                        onChangeText={(text)=>setAge(text)}
                    />

                    <InputText 
                        required
                        title="Profile" 
                        value={image} 
                        onChangeText={(text)=>setImage(text)}
                        placeholder="Use image Link">    
                    </InputText>
                </View>
                <Pressable style={styles.saveButton} onPress={onSubmit}>
                    <Text style={styles.title}>Add</Text>
                </Pressable>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    // imageContainer:{
    //     width:'100%',
    //     height:undefined,
    //     aspectRatio:2,
    //     elevation:2,
    //     backgroundColor:'white'
    // },
    // imageProfile:{
    //     width:'100%',
    //     height: '100%',
    //     resizeMode:'contain'
    // },
    saveButton:{
        flex:1,
        padding:10,
        margin:10,
        alignItems:'center',
        backgroundColor:'skyblue',
        borderRadius:10
    },
    title:{
        fontWeight:'bold',
        fontSize:18
    }
})

export default ContactInput
